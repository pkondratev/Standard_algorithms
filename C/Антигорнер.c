﻿//Антигорнер

/*
    Перевод числа из 10-ной системы счисления в n-ю
*/

#include <stdio.h>
#define SIZE 6

void Antigorner(char *result, int len, unsigned num, unsigned base) {
    result = result + len;
    while (num) {
        *--result=(num%base<=9)?(num%base+'0'):(num%base+'A'-10);
        num/=base;
    }
}

int main()
{
  char mas[SIZE];
  char *r = mas;
  Antigorner(r, SIZE, 1194684, 16);
  printf("%s", mas);
}