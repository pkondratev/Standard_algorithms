﻿#include <stdio.h>

//Чтение файла с концом строки
void WithEOF(char *file_name);
//Чтение файла без конца строки
void WithoutEOF(char *file_name);

int main(int argc, char *argv[]) {
    WithEOF("in.txt");
    WithoutEOF("in.txt");
    return 0;
}

void WithEOF(char *file_name) {
    char c;
    FILE *f = fopen(file_name, "r");
    if (!f) {
        printf("Error!");
        return;
    }
    while ((c = fgetc(f)) != EOF) {
        putchar(c);
    }
    fclose(f);
}

void WithoutEOF(char *file_name) {
    char c;
    FILE *f = fopen(file_name, "r");
    if (!f) {
        printf("Error!");
        return;
    }
    while (!feof(f)) {
        c = fgetc(f);
        putchar(c);
    }
    fclose(f);
}