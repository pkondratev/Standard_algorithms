program project1;

uses character;

//Горнер (Перевод из n-ой в 10-ю систему счисления)
function Gorner(num : string; base : integer) : integer;
var
  i : integer;
  res : integer = 0;
begin
  for i := 1 to length(num) do
  	if num[i] in ['0'..'9'] then
      res := res * base + ord(num[i]) - ord('0')
    else
      res := res * base + ord(ToUpper(num[i])) - ord('A') + 10;
  Gorner := res;
end;

//Антигорнер (Перевод из 10-ой в n-ю систему счисления)
function AntiGorner(num : integer; base : integer) : string;
var
  i : integer;
  res : string = '';
  s : string;
begin
  while num <> 0 do
  begin
  	if num mod base <= 9 then
    begin
      res := chr(num mod base + ord('0')) + res;
    end
    else
    begin
      res := chr(num mod base + ord('A')-10) + res;
    end;
    num := num div base;
  end;
  AntiGorner := res;
end;

begin
   WriteLn(Gorner('123abc', 16));
   WriteLn(AntiGorner(1194684, 16));
   readln;
end.

